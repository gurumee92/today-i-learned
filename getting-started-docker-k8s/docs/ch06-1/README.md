# 시작하세요! (도커/)쿠버네티스 스터디 1회차

## 학습 목표 

책 "시작하세요! 도커 / 쿠버네티스"의 6장의 3절까지 공부한다. 이번 장에서 배울 내용은 크게 다음과 같다.

* Pod
* ReplicaSet

## 쿠버네티스를 시작하기 전에 알아둘 것

쿠버네티스의 대부분의 리소스는 "오브젝트"라 불리는 형태로 관리한다. 여러 오브젝트가 있지만 대표적으로는 다음이 있다.

* Pod
* ReplicaSet
* Deployment
* Service
* StatefulSet
* DaemonSet
* PersistentVolume
* PersistentVolumeClaim
* Secret
* ConfigMap
* Namespace
* ServiceAccount
* Role
* RoleBinding
* ClusterRole
* ClusterRoleBinding

또한 쿠버네티스는 여러 컴포넌트로 구성되어 있다. 모든 노드는 다음이 구성된다.

* kubelet
* 프록시 kube-proxy
* Docker 같은 컨테이너 런타임 도구

그리고 컨트롤플레인노드는 다음의 컴포넌트가 구동된다.

* API 서버(kube-apiserver)
* 컨트롤러 매니저(kube-controller-manager)
* 스케줄러(kube-scheduler)
* DNS서버(coreDNS)

각 노드간 통신을 위해서 컨테이너 네트워크 인터페이스 (`CNI`)가 필요하다. 대표적으로 `calico`, `flannel` 등이 있다.

## Pod

### Pod 맛보기

`Pod`는 1개 이상의 컨테이너로 구성된 컨테이너 집합이며, 쿠버네티스의 가작 작은 빌딩 블럭이다. 먼저 `nginx` 컨테이너를 구동하는 `Pod`를 구성한다. 다음 명령어로 `Pod` 1개를 만들어보자.

```bash
# kubectl run <Pod 이름> --image=<이미지> --port=<포트>
$ kubectl run nginx --image=nginx:latest --port=80
pod/nginx created
```

`Pod`가 만들어졌다. 한 번 만들어졌는지 확인해보자. 다음 명령어를 입력한다.

```bash
$ kubectl get pod
NAME    READY   STATUS    RESTARTS   AGE
nginx   1/1     Running   0          49s
```

`Pod`의 이름인 "nginx"가 "Running" 상태인 것을 확인할 수 있다. "Running" 상태는  `Pod` 내 모든 컨테이너가 준비되었다는 뜻이며, "READY"에서 그 정보를 확인할 수 있다. 자세한 내용을 알아보자. 다음 명령어를 입력한다.

```bash
$ kubectl describe pod nginx
Name:         nginx
Namespace:    default
Priority:     0
Node:         minikube/192.168.49.2
Start Time:   Tue, 08 Feb 2022 18:54:32 +0900
Labels:       run=nginx
Annotations:  <none>
Status:       Running
IP:           172.17.0.3
IPs:
  IP:  172.17.0.3
Containers:
  nginx:
    Container ID:   docker://5b4339995cada28d7e5bbbe11c882309442951da2effbf9ae09e18a0574bce68
    Image:          nginx:latest
    Image ID:       docker-pullable://nginx@sha256:2834dc507516af02784808c5f48b7cbe38b8ed5d0f4837f16e78d00deb7e7767
    Port:           80/TCP
    Host Port:      0/TCP
    State:          Running
      Started:      Tue, 08 Feb 2022 18:54:57 +0900
    Ready:          True
    Restart Count:  0
    Environment:    <none>
    Mounts:
      /var/run/secrets/kubernetes.io/serviceaccount from kube-api-access-k7ggm (ro)
Conditions:
  Type              Status
  Initialized       True 
  Ready             True 
  ContainersReady   True 
  PodScheduled      True 
Volumes:
  kube-api-access-k7ggm:
    Type:                    Projected (a volume that contains injected data from multiple sources)
    TokenExpirationSeconds:  3607
    ConfigMapName:           kube-root-ca.crt
    ConfigMapOptional:       <nil>
    DownwardAPI:             true
QoS Class:                   BestEffort
Node-Selectors:              <none>
Tolerations:                 node.kubernetes.io/not-ready:NoExecute op=Exists for 300s
                             node.kubernetes.io/unreachable:NoExecute op=Exists for 300s
Events:
  Type    Reason     Age   From               Message
  ----    ------     ----  ----               -------
  Normal  Scheduled  2m7s  default-scheduler  Successfully assigned default/nginx to minikube
  Normal  Pulling    2m6s  kubelet            Pulling image "nginx:latest"
  Normal  Pulled     103s  kubelet            Successfully pulled image "nginx:latest" in 23.4284007s
  Normal  Created    103s  kubelet            Created container nginx
  Normal  Started    102s  kubelet            Started container nginx
```

여러 정보를 확인할 수 있다. 이제 `Pod`를 삭제해보자. 터미널에 다음을 입력한다.

```bash
$ kubectl delete pod nginx
pod "nginx" deleted
```

이러면 삭제된 것이다. 이렇게 명령어로 리소스를 구성할 수 있지만 보통은 YAML 파일로 생성할 리소스를 저장하는 편이다. 여기서는 `Pod`를 구성하는 YAML 파일을 손쉽게 구성하는 방법을 알아보자. 다음 명령어로 `Pod`를 구성하는 YAML 파일을 작성할 수 있다. 

```bash
# kubectl pod을 구성하는 파일을 만든다.
# kubectl run <Pod 이름> --image=<이미지> --port=<포트> --dry-run=client -o yaml > 생성할 파일
$ kubectl run nginx --image=nginx:latest --port=80 --dry-run=client -o yaml > ex06_01_nginx-pod.yaml
```

`--dry-run=client` 옵션을 붙이면 `Pod`는 생성되지 않으며, 리소스가 잘 생성되는지만 확인한다. 또한 `-o yaml` 옵션은 해당 리소스의 구성을 YAML 형식으로 출력해준다. 즉 `Pod`의 구성을 YAML 형식으로 출력하는데 `> ex06_01_nginx-pod.yaml` 의해서 그 출력문이 `ex06_01_nginx-pod.yaml`로 저장된다. 해당 파일의 내용은 다음과 같아진다.

src/ch06/ex06_01_nginx-pod.yaml
```yml
apiVersion: v1                  # (1)
kind: Pod                       # (2)
metadata:                       # (3)
  creationTimestamp: null
  labels:
    run: nginx
  name: nginx                   
spec:                           # (4)
  containers:
  - image: nginx:latest
    name: nginx
    ports:
    - containerPort: 80
    resources: {}
  dnsPolicy: ClusterFirst
  restartPolicy: Always
status: {}
```

여기서는 중요한 것만 살펴보자.

* (1) apiVersion: 생성할 리소스의 오브젝트 API 버전을 작성하는 곳
* (2) kind: 생성할 리소스의 오브젝트 종류를 작성하는 곳
* (3) metadata: 생성할 리소스의 Label, Annotation 등의 메타 정보를 작성하는 곳
* (4) spec: 생성할 리소스의 자세한 정보를 작성하는 곳. 컨테이너 이미지, 컨테이너 이름, 컨테이너 포트 등을 작성할 수 있다.

이렇게 YAML 파일을 만들고 다음 명령어를 통해서 리소스를 구성할 수 있다.

```bash
$ kubectl create -f ex06_01_nginx-pod.yaml 
pod/nginx created
```

또한 다음 명령어로 현재 구성된 리소스 내용을 제거할 수 있다.

```bash
$ kubectl delete -f ex06_01_nginx-pod.yaml
pod "nginx" deleted
```

파일에 변경 사항이 있을 때 이를 적용하고 싶으면 다음 명령어를 사용할 수 있다.

```bash
$ kubectl apply -f ex06_01_nginx-pod.yaml
```

`Pod`를 구성할 때 이 명령어 토대로 파일을 만드는 습관을 들이면 매우 좋다. 

### 2개 이상의 컨테이너를 구동하는 Pod 만들기

아까 `Pod`는 1개 이상의 컨테이너 집합체라고 했다. 이번에는 2개의 컨테이너를 갖는 `Pod`를 구성한다. 다음 YAML 파일을 만든다.

ex06_02_nginx-pod-with-sidecar.yaml
```yml
apiVersion: v1
kind: Pod
metadata:
  name: nginx-with-sidecar
spec:
  containers:
  - image: nginx:latest
    name: nginx
    ports:
    - containerPort: 80
  - image: rancher/curl
    name: sidecar
    command: ["sleep", "9999999"]
```

위 파일을 만들 때 나는 1개의 컨테이너를 갖는 `Pod` 구성을 명령어로 만들고 필요한 부분만 수정했다. `Pod`를 생성해보자.

```bash
$ kubectl create -f ex06_02_nginx-pod-with-sidecar.yaml
```

그 후 다음 명령어로 상태를 확인해보자.

```bash
$ kubectl get pod 
NAME                 READY   STATUS    RESTARTS   AGE
nginx-with-sidecar   2/2     Running   0          15s
```

이번엔 "READY"가 "2/2"가 된 것을 확인할 수 있다. 즉 "READY"는 `Pod`내에 실행되는 컨테이너 개수를 나타낸다. 현재 `Pod`의 내부는 다음과 같다.

![01](./01.png)

이제 `sidecar` 컨테이너를 통해서 `nginx` 컨테이너에 GET 요청을 진행해보자. 터미널에 다음을 입력한다.

```bash
# kubectl exec <pod name> -c <container name> -- <전달할 명령어>
$ kubectl exec nginx-with-sidecar -c sidecar -- curl localhost
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100   615  100   615    0     0    615      0  0:00:01 --:--:--  0:00:01  600k
<!DOCTYPE html>
<html>
<head>
<title>Welcome to nginx!</title>
<style>
html { color-scheme: light dark; }
body { width: 35em; margin: 0 auto;
font-family: Tahoma, Verdana, Arial, sans-serif; }
...
```

`kubectl exec` 명령어는 `Pod`, `Pod`내 컨테이너에게 실행할 명령어를 전달할 수가 있다. 근데 여기서 이상한 점이 있다. 바로 "sidecar" 컨테이너에 전달한 명령어 `curl localhost`이다. 각 컨테이너는 격리되어 있을텐데 어떻게 이런 통신이 가능한 것일까? 결론부터 말하자면 `Pod`내 컨테이너끼리는 같은 리눅스 네임스페이스를 공유하기 때문에 이것이 가능하다.

### Pod의 컨테이너간 통신하는 쿠버네티스의 마법

이제 이러한 마법을 파헤쳐보자. 이 마법을 알기 위해서는 `Pod`가 실행되는 워커 노드에 접속 필요하다. 접속 후에, 먼저 `Pod` 이름으로 실행되는 컨테이너들을 찾아보자.

```bash
$ docker ps | grep "nginx-with-sidecar"
29b97ccd6b36   rancher/curl           "sleep 9999999"          10 minutes ago      Up 10 minutes                k8s_sidecar_nginx-with-sidecar_default_613e0fa4-6dd6-462e-abd2-c2569a7f4572_0
fd13850e4c7e   nginx                  "/docker-entrypoint.…"   10 minutes ago      Up 10 minutes                k8s_nginx_nginx-with-sidecar_default_613e0fa4-6dd6-462e-abd2-c2569a7f4572_0
04292211a111   k8s.gcr.io/pause:3.5   "/pause"                 10 minutes ago      Up 10 minutes                k8s_POD_nginx-with-sidecar_default_613e0fa4-6dd6-462e-abd2-c2569a7f4572_0
```

어라 3개가 뜬다. image를 봤을 때 "sidecar", "nginx" 컨테이너는 알겠는데 저 "/pause" 커맨드를 날리고 있는 이상한 컨테이너가 남아있다. 이 녀석이 바로 `Pod`내 컨테이너 간 통신을 연결해주는 `Pause`라는 친구이다. 실제로 "sidecar"와 "nginx"의 네트워크 모드를 찾으면 다음과 같다.

```bash
# sidecar의 네트워크 모드 검색
$ docker inspect 29b97ccd6b36 | grep NetworkMode
            "NetworkMode": "container:04292211a1110c365213510d92c5a4753c0b2e536bde98f989f0ee58fc0a5f81",

# nginx의 네트워크 모드 검색
$ docker inspect fd13850e4c7e | grep NetworkMode
            "NetworkMode": "container:04292211a1110c365213510d92c5a4753c0b2e536bde98f989f0ee58fc0a5f81",
```

같은 값인 "conainer:04292211a111...."을 볼 수 있다. "04292211a111"로 시작하는 이상한 해시 값은 바로 `Pause`의 컨테이너 ID를 의미한다. 컨테이너 간 연결하는 네트워크가 바로 `Pause`라는 것이다! 즉 `Pod`의 내부는 실제적으로 다음과 같아서 서로 통신이 가능하다.

![02](./02.png)

## ReplicaSet

### ReplicaSet 맛보기

`ReplicaSet`이란 여러 개의 `Pod`를 구성할 수 있는 오브젝트이다. 물론 여러 개의 포드를 하나 하나 만들 수 있다. 하지만 `ReplicaSet`을 이용하면 훨씬 더 빠르고 쉽게 구성할 수 있다. 다음 YAML 파일을 만들어보자.

src/ch06/ex06_03_nginx-rs.yaml
```yml
apiVersion: apps/v1
kind: ReplicaSet
metadata:
  name: nginx-rs
spec:
  replicas: 3
  selector:             
    matchLabels:        # (1)
      app: nginx
####################################################    
  template:
    metadata:
      labels:           # (2)
        app: nginx
    spec:
      containers:
      - image: nginx:latest
        name: nginx
        ports:
        - containerPort: 80
```

여기서 주석으로 줄지은 부분 위로는 `ReplicaSet`구성을, 아래로는 `Pod` 구성을 하는 것이라고 보면 된다. 중요한 것은 바로 `selector.matchLabels`이다. 라빌 키가 "app", 라벨 값이 "nginx"인 `Pod`의 개수를 이 `ReplicaSet`이 관리한다고 명시하는 것이라 보면 된다. 실제 `selector.matchLabels`과 `Pod`의 `metadata.lables` 밑에 라벨이 동일한 것을 확인할 수 있다.

생성된 `ReplicaSet`의 상태를 확인하려면 터미널에 다음을 입력하면 된다.

```bash
$ kubectl get replicasets # 혹은 kubectl get rs
NAME       DESIRED   CURRENT   READY   AGE
nginx-rs   3         3         3       7m4s
```

"DESIRED"는 원하는 개수, "CURRENT"는 현재 클러스터에 배포된 `POD`의 개수이다. "READY"는 현재 클러스터에 배포된 `POD` 중 준비된 `POD`의 개수이다. 이제 `Pod`를 살펴보자

```bash
$ kubectl get pod
NAME             READY   STATUS    RESTARTS   AGE
nginx-rs-nrgzh   1/1     Running   0          8m47s
nginx-rs-nvxpc   1/1     Running   0          8m47s
nginx-rs-zbjvb   1/1     Running   0          8m47s
```

`ReplicaSet`의 이름 "nginx-rs"가 prefix로 붙고 그 후에 이상한 해시값들이 존재한다. `ReplicaSet`의 목적은 지정된 Label을 갖는 `Pod`의 개수를 유지하는 것이다. 그림을 통해서 알아보자. 현재 상태는 다음과 같다.

![03](./03.png)

여기서 만약 `Pod` 1개를 강제로 종료하면 어떻게 될까? 한 번 지워보자.

```bash
$ kubectl delete pod nginx-rs-nrgzh # 3개 중 1개 선택
```

그 후 상태를 지켜본다.

```bash
$ kubectl get pod -w
NAME             READY   STATUS              RESTARTS   AGE
nginx-rs-6jzqr   0/1     ContainerCreating   0          3s
nginx-rs-nvxpc   1/1     Running             0          10m
nginx-rs-zbjvb   1/1     Running             0          10m
nginx-rs-6jzqr   1/1     Running             0          4s
```

삭제되자마자 "nginx-rs-6jzqr"이 바로 생겨났다. 그래서 3개를 계속 유지한다. 현재 상태는 다음과 같다.

![04](./04.png)

이번에는 그럼 관리하고 있는 "Label"을 풀어버리면 어떻게 될까? 먼저 터미널에 다음을 입력해보자.

```bash
$ kubectl get pod --show-labels
NAME             READY   STATUS    RESTARTS   AGE    LABELS
nginx-rs-6jzqr   1/1     Running   0          104s   app=nginx
nginx-rs-nvxpc   1/1     Running   0          12m    app=nginx
nginx-rs-zbjvb   1/1     Running   0          12m    app=nginx
```

여기서 `Pod` 1개의 라벨을 변경해보자.

```bash
# kubectl label pod <pod name>     <key=value> [--overwrite 있어야 덮어쓰기 가능]
$ kubectl label pod nginx-rs-6jzqr app=changed --overwrite
pod/nginx-rs-6jzqr labeled
```

그 후 `Pod`가 어떻게 되나 확인해보자.

```bash
$ kubectl get pod --show-labels
NAME             READY   STATUS    RESTARTS   AGE     LABELS
nginx-rs-6jzqr   1/1     Running   0          4m34s   app=changed
nginx-rs-dtvb5   1/1     Running   0          21s     app=nginx
nginx-rs-nvxpc   1/1     Running   0          14m     app=nginx
nginx-rs-zbjvb   1/1     Running   0          14m     app=nginx
```

역시 "app=nginx"가 3개로 맞춰졌다. 현재 상태는 다음과 같다.

![05](./05.png)

이제 `ReplicaSet`을 삭제해보자.

```bash
$ kubectl delete -f ex06_03_nginx-rs.yaml
replicaset.apps "nginx-rs" deleted
```

그 후 `Pod`을 보면 다음과 같다.

```bash
kubectl get pod --show-labels
NAME             READY   STATUS    RESTARTS   AGE     LABELS
nginx-rs-6jzqr   1/1     Running   0          6m11s   app=changed
```

현재 상태는 다음과 같다

![06](./06.png)

`ReplicaSet`이 관리하는 Label을 들고 있는 `Pod`들도 함께 삭제되었다. 이전에 관리하던 `Pod`이라도 Label이 달라지면, `ReplicaSet`은 관리하지 않는 것을 알 수 있다.

## 질문

* Q1. NGINX 5xx 에러 시, 사이드 카로 reload 하는 컨테이너 붙이는 방법도 있을텐데 왜 안할까?
* Q2. ReplicaSet selector에서 matchExpressions 사용하는 리소스가 있는가?

